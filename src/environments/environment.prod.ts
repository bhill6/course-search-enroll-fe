export const environment = {
  production: true,
  version: '1.2.4',
  apiPlannerUrl: '/api/planner/v1',
  apiSearchUrl: '/api/search/v1',
  apiEnrollUrl: '/api/enroll/v1',
  apiDarsUrl: '/api/dars',
  apiDarsData: '/api/data',
  snackbarDuration: 4000,
};
