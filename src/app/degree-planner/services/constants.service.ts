import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { environment } from './../../../environments/environment';
import { forkJoinWithKeys } from '@app/degree-planner/shared/utils';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import { map, tap } from 'rxjs/operators';
import { SubjectDescription, SubjectCodesTo } from '@app/core/models/course';
import { StudentInfo } from '@app/core/models/student-info';
import { of, Observable } from 'rxjs';

export interface ConstantData {
  studentInfo: Partial<StudentInfo>;
  subjectDescriptions: SubjectCodesTo<SubjectDescription>;
}

@Injectable({ providedIn: 'root' })
export class ConstantsService implements Resolve<ConstantData> {
  private wasLoaded = false;
  private version = environment.version;
  private constants: ConstantData = {
    studentInfo: {},
    subjectDescriptions: {},
  };

  constructor(private api: DegreePlannerApiService) {}

  public getStudentInfo() {
    return this.constants.studentInfo;
  }

  public isUndergrad(): boolean {
    const info = this.getStudentInfo();
    if (info && info.primaryCareer) {
      return info.primaryCareer.careerCode === 'UGRD';
    } else {
      return false;
    }
  }

  public allSubjectDescriptions() {
    return this.constants.subjectDescriptions;
  }

  public getVersion(): string {
    return this.version;
  }

  public subjectDescription(subjectCode: string) {
    const descriptions = this.constants.subjectDescriptions[subjectCode];

    if (descriptions === undefined) {
      return { short: '', long: '' };
    } else {
      return descriptions;
    }
  }

  public resolve(): Observable<ConstantData> {
    if (this.wasLoaded) {
      return of(this.constants);
    }

    const studentInfo = this.api.getStudentInfo();

    const subjectDescriptions = forkJoinWithKeys({
      short: this.api.getSubjectShortDescriptions(),
      longByTerm: this.api.getSubjectLongDescriptions(),
    }).pipe(
      map(({ short, longByTerm }) => {
        const long = {};
        const anyTerm = longByTerm['0000'];
        for (const subject of anyTerm) {
          long[subject.subjectCode] = subject.formalDescription;
        }
        return { short, long: long as SubjectCodesTo<string> };
      }),
      map(({ short, long }) => {
        const allCodes = [...Object.keys(short), ...Object.keys(long)];
        const descriptions = {};
        for (const subjectCode of allCodes) {
          if (descriptions[subjectCode] === undefined) {
            descriptions[subjectCode] = {
              short: short[subjectCode] || '',
              long: long[subjectCode] || '',
            };
          }
        }
        return descriptions as SubjectCodesTo<SubjectDescription>;
      }),
    );

    return forkJoinWithKeys({
      studentInfo,
      subjectDescriptions,
    }).pipe(
      tap(constants => ((this.wasLoaded = true), (this.constants = constants))),
    );
  }
}
