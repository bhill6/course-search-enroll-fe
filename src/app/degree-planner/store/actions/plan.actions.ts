import { Action } from '@ngrx/store';
import { DegreePlan } from '@app/core/models/degree-plan';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import { YearMapping } from '@app/core/models/year';

export enum PlanActionTypes {
  InitialLoadRequest = '[Plan] Initial Load (Request)',
  InitialLoadSuccess = '[Plan] Initial Load (Success)',

  SwitchPlan = '[Plan] Switch',
  SwitchPlanSuccess = '[Plan] Switch (Success)',

  CreatePlan = '[Plan] Create',
  CreatePlanSuccess = '[Plan] Create (Success)',

  DeletePlan = '[Plan] Delete',
  DeletePlanSuccess = '[Plan] Delete (Success)',

  PlanError = '[Plan] Error',

  MakePlanPrimary = '[Plan] Make Plan Primary',
  MakePlanPrimarySuccess = '[Plan] Make Plan Primary (Success)',
  MakePlanPrimaryFailure = '[Plan] Make Plan Primary (Failure)',

  ChangePlanName = '[Plan] Change Plan Name',
  ChangePlanNameSuccess = '[Plan] Change Plan Name (Success)',
  ChangePlanNameFailure = '[Plan] CHange Plan Name (Failure)',

  ChangeGradeVisibility = '[Plan] Change Grade Visibility',
}

export class InitialLoadRequest implements Action {
  public readonly type = PlanActionTypes.InitialLoadRequest;
}

export class InitialLoadSuccess implements Action {
  public readonly type = PlanActionTypes.InitialLoadSuccess;
  constructor(public payload: DegreePlannerState) {}
}

export class SwitchPlan implements Action {
  public readonly type = PlanActionTypes.SwitchPlan;
  constructor(public payload: { newVisibleRoadmapId: number }) {}
}

export class ChangeGradeVisibility implements Action {
  public readonly type = PlanActionTypes.ChangeGradeVisibility;
  constructor(public visibility: boolean) {}
}

export class SwitchPlanSuccess implements Action {
  public readonly type = PlanActionTypes.SwitchPlanSuccess;
  constructor(
    public payload: {
      visibleDegreePlan: DegreePlan;
      visibleYears: YearMapping;
    },
  ) {}
}

export class CreatePlan implements Action {
  public readonly type = PlanActionTypes.CreatePlan;
  constructor(public payload: { name: string; primary: boolean }) {}
}

export class CreatePlanSuccess implements Action {
  public readonly type = PlanActionTypes.CreatePlanSuccess;
  constructor(public payload: { newPlan: DegreePlan; newYears: YearMapping }) {}
}

export class DeletePlan implements Action {
  public readonly type = PlanActionTypes.DeletePlan;
  constructor(public payload: { roadmapId: number }) {}
}

export class DeletePlanSuccess implements Action {
  public readonly type = PlanActionTypes.DeletePlanSuccess;
  constructor(public payload: { roadmapId: number }) {}
}

export class PlanError implements Action {
  public readonly type = PlanActionTypes.PlanError;
  constructor(
    public payload: { message: string; duration: number; error: any },
  ) {}
}

export class MakePlanPrimary implements Action {
  public readonly type = PlanActionTypes.MakePlanPrimary;
}

export class MakePlanPrimarySuccess implements Action {
  public readonly type = PlanActionTypes.MakePlanPrimarySuccess;
}

export class MakePlanPrimaryFailure implements Action {
  public readonly type = PlanActionTypes.MakePlanPrimaryFailure;
}

export class ChangePlanName implements Action {
  public readonly type = PlanActionTypes.ChangePlanName;
  constructor(public payload: { roadmapId: number; newName: string }) {}
}

export class ChangePlanNameSuccess implements Action {
  public readonly type = PlanActionTypes.ChangePlanNameSuccess;
  constructor(public payload: { roadmapId: number; newName: string }) {}
}

export class ChangePlanNameFailure implements Action {
  public readonly type = PlanActionTypes.ChangePlanNameFailure;
  constructor(public payload: { roadmapId: number; oldName: string }) {}
}
