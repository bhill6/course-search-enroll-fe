import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveCourseConfirmDialogComponent } from './remove-course-confirm-dialog.component';

describe('RemoveCourseConfirmDialogComponent', () => {
  let component: RemoveCourseConfirmDialogComponent;
  let fixture: ComponentFixture<RemoveCourseConfirmDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RemoveCourseConfirmDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveCourseConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
