import { Component } from '@angular/core';

@Component({
  selector: 'cse-ie11-warning-dialog',
  templateUrl: './ie11-warning-dialog.component.html',
})
export class IE11WarningDialogComponent {}
