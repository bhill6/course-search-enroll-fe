import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { CourseDetails } from '@app/core/models/course-details';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';

@Component({
  selector: 'cse-course-details-dialog',
  templateUrl: './course-details-dialog.component.html',
})
export class CourseDetailsDialogComponent implements OnInit {
  private subjectCode: string;
  private courseId: string;
  public type: 'course' | 'search' | 'saved';
  public data$: Observable<CourseDetails>;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    data: {
      subjectCode: string;
      courseId: string;
      type: CourseDetailsDialogComponent['type'];
    },
    private api: DegreePlannerApiService,
  ) {
    this.subjectCode = data.subjectCode;
    this.courseId = data.courseId;
    this.type = data.type;
  }

  public ngOnInit() {
    this.data$ = this.api.getCourseDetails(this.subjectCode, this.courseId);
  }
}
