import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Course } from '@app/core/models/course';
import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core/core.module';

import { CourseItemComponent } from './course-item.component';

describe('CourseItemComponent', () => {
  let component: CourseItemComponent;
  let fixture: ComponentFixture<CourseItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        CoreModule,
        SharedModule,
      ],
      declarations: [CourseItemComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemComponent);
    component = fixture.componentInstance;
    const course: Course = {
      studentEnrollmentStatus: 'Enrolled',
      id: null,
      courseId: '022973',
      termCode: '1234',
      topicId: 0,
      title: 'A Wisconsin Experience Seminar',
      subjectCode: '270',
      catalogNumber: '125',
      credits: 0,
      creditMin: 0,
      creditMax: 0,
      grade: 'A',
      classNumber: '53611',
      courseOrder: 0,
      honors: 'N',
      waitlist: 'N',
      relatedClassNumber1: null,
      relatedClassNumber2: null,
      classPermissionNumber: null,
      sessionCode: null,
      validationResults: [],
      enrollmentResults: [],
      pendingEnrollments: [],
      details: null,
      classMeetings: null,
      enrollmentOptions: null,
      packageEnrollmentStatus: null,
      creditRange: null,
    };
    component.course = course;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
