import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { filter, map, distinctUntilChanged, pairwise } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import {
  OpenCourseSearch,
  CloseCourseSearch,
} from '@app/degree-planner/store/actions/ui.actions';
import * as actions from '@app/degree-planner/store/actions/course.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { PlannedTerm, PlannedTermNote } from '@app/core/models/planned-term';
import { Course, pickCreditAmountFromCourse } from '@app/core/models/course';
import {
  NotesDialogComponent,
  NotesDialogData,
} from '@app/degree-planner/dialogs/notes-dialog/notes-dialog.component';
import * as utils from '@app/degree-planner/shared/utils';
import { TermCode, Era } from '@app/shared/term-codes/termcode';
import { ConfirmDialogComponent } from '@app/shared/dialogs/confirm-dialog/confirm-dialog.component';
import { MediaMatcher } from '@angular/cdk/layout';
import { ConstantsService } from '../services/constants.service';
import { CreditOverloadDialogComponent } from '../dialogs/credit-overload-dialog/credit-overload-dialog.component';
import { LiveAnnouncer } from '@angular/cdk/a11y';

const isntUndefined = <T>(thing: T | undefined): thing is T => {
  return thing !== undefined;
};

const sumCredits = (
  courses: ReadonlyArray<{ creditMin?: number; creditMax?: number }>,
) => {
  const min = courses.reduce((sum, course) => {
    return sum + (course.creditMin !== undefined ? course.creditMin : 0);
  }, 0);

  const max = courses.reduce((sum, course) => {
    return sum + (course.creditMax !== undefined ? course.creditMax : 0);
  }, 0);

  return { min, max };
};

// Both the summer and fall/spring undergrad credit limits are INCLUSIVE.
const SUMMER_CREDIT_LIMIT = 12;
const FALL_SPRING_CREDIT_LIMIT = 18;

const maximumAllowedCreditsForTerm = (termCode: TermCode) => {
  switch (termCode.termName) {
    case 'fall':
    case 'spring':
      return FALL_SPRING_CREDIT_LIMIT;
    case 'summer':
      return SUMMER_CREDIT_LIMIT;
  }
};

@Component({
  selector: 'cse-term-container',
  templateUrl: './term-container.component.html',
  styleUrls: ['./term-container.component.scss'],
})
export class TermContainerComponent implements OnInit, OnDestroy {
  @Input() termCode: TermCode;

  public term$: Observable<PlannedTerm>;
  public note$: Observable<PlannedTermNote | undefined>;
  public dropZoneIds$: Observable<string[]>;
  public tooManyCredits$: Observable<boolean>;

  public termSubscription: Subscription;
  public isPrimarySubscription: Subscription;
  public activeTermHasNotOffered: boolean;
  // List of courses pulled for the Observable
  public isPrimaryPlan: boolean;
  public plannedCourses: ReadonlyArray<Course>;
  public enrolledCourses: ReadonlyArray<Course>;
  public transferredCourses: ReadonlyArray<Course>;
  public transferredCredits: number;
  public hasItemDraggedOver: boolean;
  public plannedCredits: string;
  public enrolledCredits: number;
  public visibleCredits: 'enrolled' | 'planned' | 'transferred';
  public courseNotOfferedInTerm: ReadonlyArray<Course>;
  public mobileView: MediaQueryList;
  public maxAllowedCredits: number;

  constructor(
    public dialog: MatDialog,
    private store: Store<{ degreePlanner: DegreePlannerState }>,
    private constants: ConstantsService,
    private announcer: LiveAnnouncer,
    mediaMatcher: MediaMatcher,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 900px)');
  }

  public ngOnInit() {
    this.hasItemDraggedOver = false;

    this.isPrimarySubscription = this.store
      .pipe(
        select(selectors.selectVisibleDegreePlan),
        filter(isntUndefined),
        map(plan => plan.primary),
      )
      .subscribe(plan => (this.isPrimaryPlan = plan));

    this.term$ = this.store.pipe(
      select(selectors.selectVisibleTerm, { termCode: this.termCode }),
      filter(isntUndefined),
      distinctUntilChanged(),
    );

    this.tooManyCredits$ = this.term$.pipe(
      map(term => {
        if (this.constants.isUndergrad()) {
          const credits = sumCredits(term.plannedCourses);
          this.maxAllowedCredits = maximumAllowedCreditsForTerm(term.termCode);
          return credits.min >= this.maxAllowedCredits;
        } else {
          return false;
        }
      }),
    );

    /**
     * Alert the user that they are adding too many credits to a term IFF:
     * 1. The user is an undergrad
     * 2. The term did not exceed the credit limit before the most recent change
     * 3. The term's minimum credit amount exceeds the term's credit limit
     */

    // Condition #1
    if (this.constants.isUndergrad()) {
      this.term$.pipe(pairwise()).subscribe(([prev, curr]) => {
        // Sanity check: don't compare two terms if they're different terms.
        if (
          prev.roadmapId !== curr.roadmapId ||
          prev.termCode.equals(curr.termCode) === false
        ) {
          return;
        }

        const prevCredits = sumCredits(prev.plannedCourses);
        const currCredits = sumCredits(curr.plannedCourses);
        const maxAllowedCredits = maximumAllowedCreditsForTerm(curr.termCode);
        const prevWasOverLimit = prevCredits.min >= maxAllowedCredits;
        const currIsUnderLimit = currCredits.min < maxAllowedCredits;
        const currHasFewerCreditsThanPrev = currCredits.min < prevCredits.min;

        if (prevWasOverLimit || currHasFewerCreditsThanPrev) {
          // Failed condition #2
          return;
        }

        if (currIsUnderLimit) {
          // Failed condition #3
          return;
        }

        this.dialog.open(CreditOverloadDialogComponent, {
          data: {
            termName: curr.termCode.termName,
            maxCredits: maxAllowedCredits,
          },
          closeOnNavigation: true,
        });
      });
    }

    this.termSubscription = this.term$.subscribe(term => {
      this.plannedCourses = term.plannedCourses;
      this.plannedCredits = this.sumCreditRange(term.plannedCourses);

      this.enrolledCourses = term.enrolledCourses;
      this.enrolledCredits = this.sumFixedCredits(term.enrolledCourses);

      this.transferredCourses = term.transferredCourses;
      this.transferredCredits = this.sumFixedCredits(term.transferredCourses);

      if (term.termCode.era === Era.Past) {
        this.visibleCredits = 'enrolled';
      } else if (term.termCode.era === Era.Active) {
        if (this.enrolledCourses.length === 0) {
          this.visibleCredits = 'planned';
        } else {
          this.visibleCredits = 'enrolled';
        }
      } else {
        this.visibleCredits = 'planned';
      }
    });

    this.note$ = this.term$.pipe(
      map(term => term.note),
      distinctUntilChanged(),
    );

    this.dropZoneIds$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      utils.yearsToDropZoneIds(),
      distinctUntilChanged(utils.compareStringArrays),
    );
  }

  ngOnDestroy() {
    this.termSubscription.unsubscribe();
    this.isPrimarySubscription.unsubscribe();
  }

  openNotesDialog(note?: PlannedTermNote) {
    if (note === undefined || note.isLoaded) {
      const termCode = this.termCode;
      const data: NotesDialogData = note
        ? {
            termCode,
            hasExistingNote: true,
            initialText: note.text,
            noteId: note.id,
          }
        : { termCode, hasExistingNote: false };
      this.dialog.open(NotesDialogComponent, { data, closeOnNavigation: true });
    }
  }

  openCourseSearch() {
    this.store.dispatch(new OpenCourseSearch(this.termCode));
  }

  changeVisibleCredits(event) {
    switch (event.index) {
      case 0:
        this.visibleCredits = 'enrolled';
        break;
      case 1:
        this.visibleCredits = 'planned';
        break;
      default:
        this.visibleCredits = 'transferred';
    }
  }

  startDrag(event) {
    const touchedCourse =
      event.source.data.subjectDescription +
      ' ' +
      event.source.data.catalogNumber;
    this.announcer.announce(`Dragging ${touchedCourse} course`, 'assertive');
  }

  drop(event: CdkDragDrop<TermCode>) {
    const newContainer = event.container.id;
    const previousContainer = event.previousContainer.id;
    const { courseId } = event.item.data as Course;
    const isCourseInPlannedCourses = this.plannedCourses.some(
      course => course.courseId === courseId,
    );

    this.hasItemDraggedOver = false;
    this.announcer.announce('Dropped course', 'assertive');

    if (newContainer !== previousContainer && isCourseInPlannedCourses) {
      this.dialog
        .open(ConfirmDialogComponent, {
          data: {
            title: `Can't add course to term`,
            confirmText: 'OK',
            dialogClass: 'alertDialog',
            text: `This course already exists in selected term`,
          },
        })
        .afterClosed();
      return;
    }

    if (newContainer === previousContainer) {
      const newIndex = event.currentIndex;
      const termCode = event.container.data;
      const { id: recordId } = event.item.data as Course;

      if (recordId !== null) {
        const action = new actions.MoveCourseInsideTerm({
          termCode,
          recordId,
          newIndex,
        });
        this.store.dispatch(action);
      }
    } else if (previousContainer.indexOf('term-') === 0) {
      // If moving from term to term

      // Get the pervious and new term code, and the record ID
      const from = event.previousContainer.data;
      const to = event.container.data;
      // FIXME: if `event.item.data` is a Course, the `id` property could be null
      // tslint:disable-next-line: no-shadowed-variable
      const { id, courseId, subjectCode } = event.item.data;
      const newIndex = event.currentIndex;
      const { classNumber } = event.item.data as Course;

      if (classNumber !== null) {
        // If moving course with packages to future term
        this.dialog
          .open(ConfirmDialogComponent, {
            data: {
              title: 'Are you sure?',
              confirmText: 'Move course',
              text: `Moving this course to a future term will remove your selected section`,
            },
          })
          .afterClosed()
          .subscribe((result: { confirmed: true } | undefined) => {
            if (result !== undefined && result.confirmed === true) {
              this.store.dispatch(
                new actions.MoveCourseBetweenTerms({
                  to,
                  from,
                  id,
                  newIndex,
                  courseId,
                  subjectCode,
                  credits: pickCreditAmountFromCourse(event.item.data),
                }),
              );
            }
          });
      } else {
        // Dispatch a new change request
        this.store.dispatch(
          new actions.MoveCourseBetweenTerms({
            to,
            from,
            id,
            newIndex,
            courseId,
            subjectCode,
            credits: pickCreditAmountFromCourse(event.item.data),
          }),
        );
      }
    } else if (previousContainer === 'saved-courses') {
      // If moving from saved courses to term

      // Get the term code from the new term dropzone's ID
      const termCode = event.container.data;
      const newIndex = event.currentIndex;
      // Pull the course data from the moved item
      // tslint:disable-next-line: no-shadowed-variable
      const { subjectCode, courseId, title, catalogNumber } = event.item.data;
      this.store.dispatch(
        new actions.AddCourse({
          courseId,
          termCode,
          subjectCode,
          title,
          catalogNumber,
          newIndex,
        }),
      );
      this.store.dispatch(
        new actions.RemoveSaveForLater({ subjectCode, courseId }),
      );
    } else if (
      previousContainer === 'queried-courses-list' &&
      newContainer.indexOf('term-') === 0
    ) {
      const termCode = event.container.data;
      const newIndex = event.currentIndex;
      this.store.dispatch(
        new actions.AddCourse({
          courseId: event.item.data.courseId,
          termCode,
          subjectCode: event.item.data.subjectCode,
          title: event.item.data.title,
          catalogNumber: event.item.data.catalogNumber,
          newIndex,
        }),
      );

      if (this.mobileView.matches) {
        this.store.dispatch(new CloseCourseSearch());
      }
    }
  }

  dragEnter(_event, _item) {
    this.hasItemDraggedOver = true;
  }

  dragExit(_event, _item) {
    this.hasItemDraggedOver = false;
  }

  sumFixedCredits(courses: ReadonlyArray<Course>): number {
    return courses.reduce(
      (sum, course) => sum + (course.credits ? course.credits : 0),
      0,
    );
  }

  sumCreditRange(courses: ReadonlyArray<Course>): string {
    const credits = { min: 0, max: 0 };
    courses.forEach(course => {
      if (course.creditMin !== undefined && course.creditMax !== undefined) {
        credits.min = credits.min + course.creditMin;
        credits.max = credits.max + course.creditMax;
      }
    });

    return credits.min === credits.max
      ? credits.min.toString()
      : `${credits.min}-${credits.max}`;
  }
}
