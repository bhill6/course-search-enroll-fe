import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'cse-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['../dialogs.scss'],
})
export class ConfirmDialogComponent {
  title: string;
  text: string[];
  dialogClass: string;
  cancelText: string;
  confirmText: string;
  confirmColor: string;

  constructor(
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
  ) {
    const {
      title = 'Are you sure?',
      text = '',
      dialogClass = '',
      cancelText = 'Cancel',
      confirmText = 'Confirm',
      confirmColor = 'primary',
    } = data;

    this.title = title;
    this.text = text;
    this.dialogClass = dialogClass;
    this.cancelText = cancelText;
    this.confirmText = confirmText;
    this.confirmColor = confirmColor;

    if (typeof text === 'string') {
      this.text = [text];
    }
  }

  confirm() {
    this.dialogRef.close({ confirmed: true });
  }

  cancel() {
    this.dialogRef.close({ confirmed: false });
  }
}
