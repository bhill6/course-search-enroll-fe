import { AlertContainerComponent } from './components/alert-container/alert-container.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { A11yModule } from '@angular/cdk/a11y';

import { GetTermDescriptionPipe } from './pipes/get-term-description.pipe';
import { AcademicYearStatePipe } from './pipes/academic-year-state.pipe';
import { AcademicYearRangePipe } from './pipes/academic-year-range.pipe';
import { CourseDetailsComponent } from './components/course-details/course-details.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { CourseDetailsDialogComponent } from '../degree-planner/dialogs/course-details-dialog/course-details-dialog.component';
import { FeedbackDialogComponent } from '../degree-planner/dialogs/feedback-dialog/feedback-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { PromptDialogComponent } from './dialogs/prompt-dialog/prompt-dialog.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {
  ShortSubjectDescription,
  LongSubjectDescription,
} from './pipes/subject-description.pipe';

import { ClickStopPropagationDirective } from '@app/shared/directives/click-stop-propigation';
import { CourseDescriptionPipe } from './pipes/course-description.pipe';
import { CreditOverloadDialogComponent } from '@app/degree-planner/dialogs/credit-overload-dialog/credit-overload-dialog.component';
import { IE11WarningDialogComponent } from '@app/degree-planner/dialogs/ie11-warning-dialog/ie11-warning-dialog.component';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';
import { SiteHelpDialogComponent } from './dialogs/site-help-dialog/site-help-dialog.component';

const modules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatTabsModule,
  MatExpansionModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatSelectModule,
  MatRadioModule,
  FlexLayoutModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatListModule,
  MatToolbarModule,
  MatDialogModule,
  MatInputModule,
  MatTooltipModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  A11yModule,
  NgxLinkifyjsModule,
];
const pipes = [
  GetTermDescriptionPipe,
  AcademicYearStatePipe,
  AcademicYearRangePipe,
  ShortSubjectDescription,
  LongSubjectDescription,
  CourseDescriptionPipe,
];

const directives = [ClickStopPropagationDirective];

@NgModule({
  imports: [modules],
  exports: [
    modules,
    pipes,
    directives,
    CourseDetailsComponent,
    AlertContainerComponent,
  ],
  entryComponents: [ConfirmDialogComponent, PromptDialogComponent],
  declarations: [
    pipes,
    directives,
    CourseDetailsComponent,
    CourseDetailsDialogComponent,
    FeedbackDialogComponent,
    CreditOverloadDialogComponent,
    IE11WarningDialogComponent,
    SiteHelpDialogComponent,
    ConfirmDialogComponent,
    PromptDialogComponent,
    AlertContainerComponent,
  ],
})
export class SharedModule {}
