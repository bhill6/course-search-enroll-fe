import { Pipe, PipeTransform } from '@angular/core';
import { ConstantsService } from '@app/degree-planner/services/constants.service';
import { CourseBase } from '@app/core/models/course';

@Pipe({ name: 'courseDescription', pure: true })
export class CourseDescriptionPipe implements PipeTransform {
  constructor(private constants: ConstantsService) {}

  transform(arg: CourseBase) {
    if (arg.studentEnrollmentStatus === 'Transfer') {
      return `${arg.subjectDescription} ${arg.catalogNumber || ''}`;
    } else {
      const { short } = this.constants.subjectDescription(arg.subjectCode);
      return `${short} ${arg.catalogNumber}`;
    }
  }
}
