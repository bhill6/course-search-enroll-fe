import { Pipe, PipeTransform } from '@angular/core';
import { YearCode } from '@app/shared/term-codes/yearcode';

@Pipe({ name: 'academicYearState' })
export class AcademicYearStatePipe implements PipeTransform {
  transform(yearCode: YearCode): string {
    if (yearCode.summer().isPast()) {
      return `Past year: ${yearCode.fromYear}-${yearCode.toYear}`;
    }

    if (yearCode.fall().isFuture()) {
      return `Future year: ${yearCode.fromYear}-${yearCode.toYear}`;
    }

    return `Active year: ${yearCode.fromYear}-${yearCode.toYear}`;
  }
}
