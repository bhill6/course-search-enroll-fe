import { RawTermCode } from './without-era';
import { YearCode } from './yearcode';

export enum Era {
  Past,
  Active,
  Future,
}

export class TermCode extends RawTermCode {
  public era: Era;
  public yearCode: YearCode;

  constructor(from: RawTermCode | string, era: Era, yearCode: YearCode) {
    super(from.toString());
    this.yearCode = yearCode;
    this.era = era;
  }

  public isPast() {
    return this.era === Era.Past;
  }

  public isActive() {
    return this.era === Era.Active;
  }

  public isFuture() {
    return this.era === Era.Future;
  }
}
