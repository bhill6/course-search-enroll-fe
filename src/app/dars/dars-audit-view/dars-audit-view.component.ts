import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, combineLatest, Subscription } from 'rxjs';
import {
  map,
  filter,
  flatMap,
  shareReplay,
  distinctUntilKeyChanged,
} from 'rxjs/operators';
import { AuditStatus, MetadataStatus } from '../store/state';
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/core/state';
import * as selectors from '../store/selectors';
import { StartLoadingAudit, RefreshMetadata } from '../store/actions';
import { AuditMetadataWithReportId } from '../models/audit-metadata';

@Component({
  selector: 'cse-dars-audit-view',
  templateUrl: './dars-audit-view.component.html',
  styleUrls: ['./dars-audit-view.component.scss'],
})
export class AuditViewComponent implements OnInit, OnDestroy {
  public metadataStatus$: Observable<MetadataStatus['status']>;
  public darsDegreeAuditReportId$: Observable<number>;
  public metadata$: Observable<AuditMetadataWithReportId>;
  public audit$: Observable<AuditStatus>;
  private metadataSubscription: Subscription;
  private auditSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private store: Store<GlobalState>,
  ) {}

  public ngOnInit() {
    /**
     * If a user opens the DARS report view directly, the list of all audit
     * metadata hasn't been loaded yet. This subscription loads the all of the
     * user's audit metadata if it hasn't been loaded yet.
     */
    this.metadataStatus$ = this.store
      .select(selectors.metadataStatus)
      .pipe(shareReplay());

    this.metadataSubscription = this.metadataStatus$.subscribe(
      metadataStatus => {
        if (metadataStatus === 'NotLoaded') {
          this.store.dispatch(new RefreshMetadata());
        }
      },
    );

    /**
     * Get the `darsDegreeAuditReportId` from the route URL. Sanitize and parse
     * the value to an integer.
     */
    this.darsDegreeAuditReportId$ = this.route.paramMap.pipe(
      map(params => params.get('darsDegreeAuditReportId')),
      filter((id): id is string => id !== null),
      map(id => parseInt(id, 10)),
      shareReplay(),
    );

    this.metadata$ = combineLatest([
      this.darsDegreeAuditReportId$,
      this.store.select(selectors.programMetadata),
      this.store.select(selectors.whatIfMetadata),
    ]).pipe(
      map(([darsDegreeAuditReportId, programMetadata, whatIfMetadata]) => {
        return (
          programMetadata[darsDegreeAuditReportId] ||
          whatIfMetadata[darsDegreeAuditReportId]
        );
      }),
      filter((metadata): metadata is AuditMetadataWithReportId => !!metadata),
      shareReplay(),
    );

    this.audit$ = this.darsDegreeAuditReportId$.pipe(
      flatMap(darsDegreeAuditReportId => {
        return this.store.select(selectors.getAudit, darsDegreeAuditReportId);
      }),
      shareReplay(),
    );

    this.auditSubscription = combineLatest([
      this.metadata$.pipe(distinctUntilKeyChanged('darsDegreeAuditReportId')),
      this.audit$.pipe(distinctUntilKeyChanged('status')),
    ]).subscribe(([metadata, audit]) => {
      if (audit.status === 'NotLoaded') {
        this.store.dispatch(new StartLoadingAudit(metadata));
      }
    });
  }

  public ngOnDestroy() {
    this.metadataSubscription.unsubscribe();
    this.auditSubscription.unsubscribe();
  }
}
