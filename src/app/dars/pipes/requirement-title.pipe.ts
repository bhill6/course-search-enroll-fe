import { Pipe, PipeTransform } from '@angular/core';
import { Requirement, ContentType } from '../models/audit/requirement';

@Pipe({ name: 'requirementTitle', pure: true })
export class RequirementTitlePipe implements PipeTransform {
  transform(requirement: Requirement): string {
    // Get any title lines
    const titleLines = requirement.requirementContents.filter(
      (r): r is ContentType =>
        r.contentType === 'okRequirementTitle' ||
        r.contentType === 'hText' ||
        r.contentType === 'noRequirementTitle',
    );

    // Concat all the lines together, removing whitespace
    return titleLines.reduce((acc, { lines }) => {
      return `${acc} ${lines.join(' ')}`.trim();
    }, '');
  }
}
