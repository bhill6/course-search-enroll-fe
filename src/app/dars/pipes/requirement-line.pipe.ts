import { Pipe, PipeTransform } from '@angular/core';
import { ContentType } from '../models/audit/requirement';

@Pipe({ name: 'requirementLine' })
export class RequirementLinePipe implements PipeTransform {
  transform(lines: string[], type: ContentType['contentType']): string[] {
    switch (type) {
      case 'okSubrequirementTLine':
      case 'noSubrequirementTLine': {
        const singleLine = lines.join(' ').trim();
        const matches = singleLine.match(
          /^((IP)|(IN-P)|(PL)|(R)|(<>)|\+|\-|\*)+(?![a-zA-Z])/g,
        );

        // If symbol matches are found return the single line without the matches
        // Else return just the single line
        return matches && matches.length > 0
          ? [singleLine.substr(matches[0].length)]
          : [singleLine];
      }
    }

    return lines;
  }
}
