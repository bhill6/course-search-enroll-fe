import { Pipe, PipeTransform } from '@angular/core';
import { AuditSymbol } from '../models/audit-symbols';
import { AuditSymbolsService } from '../services/audit-symbols.service';

@Pipe({ name: 'courseGrade', pure: true })
export class CourseGradePipe implements PipeTransform {
  constructor(public symbols: AuditSymbolsService) {}

  public transform(grade: string | null): AuditSymbol | undefined {
    return this.symbols.getByTaxonomy('grade').find(s => s.text === grade);
  }
}
