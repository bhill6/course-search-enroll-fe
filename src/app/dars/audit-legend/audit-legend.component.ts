import { Component } from '@angular/core';
import { AuditSymbol } from '../models/audit-symbols';
import { AuditSymbolsService } from '../services/audit-symbols.service';

@Component({
  selector: 'cse-audit-legend',
  templateUrl: './audit-legend.component.html',
  styleUrls: ['./audit-legend.component.scss'],
})
export class AuditLegendComponent {
  constructor(public symbols: AuditSymbolsService) {}

  public auditCourseColumns: string[] = ['symbol', 'description'];
  public legends: { title: string; symbols: AuditSymbol[] }[] = [
    {
      title: 'Course Symbols',
      symbols: this.symbols.getByTaxonomy('course'),
    },
    {
      title: 'Grade Symbols',
      symbols: this.symbols.getByTaxonomy('grade'),
    },
    {
      title: 'Requirement / Sub-requirement Information',
      symbols: [
        ...this.symbols.getByTaxonomy('requirement'),
        ...this.symbols.getByTaxonomy('subrequirement'),
      ],
    },
    {
      title: 'Exception Symbols',
      symbols: this.symbols.getByTaxonomy('exception'),
    },
  ];
}
