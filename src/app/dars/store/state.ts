import { Alert } from './../../core/models/alert';
import { AuditMetadata, AuditMetadataMap } from '../models/audit-metadata';
import { DegreePlan } from '@app/core/models/degree-plan';
import { Audit } from '../models/audit/audit';
import { StudentDegreeProgram } from '../models/student-degree-program';

export type MetadataStatus =
  | { status: 'Error'; message: string }
  | { status: 'NotLoaded' }
  | { status: 'Loading' }
  | {
      status: 'Loaded';
      outstanding: { program: number; whatIf: number };
      pending: { program: number; whatIf: number };
      programMetadata: AuditMetadataMap;
      whatIfMetadata: AuditMetadataMap;
    };

export type AuditStatus =
  | { status: 'Error'; message: string }
  | { status: 'NotLoaded' }
  | { status: 'Loading'; metadata: AuditMetadata }
  | { status: 'Loaded'; metadata: AuditMetadata; audit: Audit };

export interface DARSState {
  hasLoaded: boolean;
  degreePlans: DegreePlan[];
  degreePrograms: StudentDegreeProgram[];
  metadata: MetadataStatus;
  audits: { [darsDegreeAuditReportId: number]: AuditStatus };
  alerts: Alert[];
}

export const INITIAL_DARS_STATE: DARSState = {
  hasLoaded: false,
  degreePlans: [],
  degreePrograms: [],
  metadata: { status: 'NotLoaded' },
  audits: {},
  alerts: [],
};
