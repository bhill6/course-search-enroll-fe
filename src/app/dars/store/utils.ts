import {
  AuditMetadataMap,
  AuditMetadata,
  AuditMetadataWithReportId,
} from '../models/audit-metadata';
import { StudentDegreeProgram } from '../models/student-degree-program';

interface SortedMetadataTuple {
  pending: { program: number; whatIf: number };
  programMetadata: AuditMetadataMap;
  whatIfMetadata: AuditMetadataMap;
}

const isFromDegreeProgram = (md: AuditMetadata, pg: StudentDegreeProgram) => {
  return (
    md.darsDegreeProgramCode === pg.darsDegreeProgramCode &&
    md.darsInstitutionCode === pg.darsInstitutionCode
  );
};

export const groupAuditMetadata = (
  metadata: readonly AuditMetadata[],
  degreePrograms: readonly StudentDegreeProgram[],
): SortedMetadataTuple => {
  const empty: SortedMetadataTuple = {
    pending: { program: 0, whatIf: 0 },
    programMetadata: {},
    whatIfMetadata: {},
  };

  return metadata.reduce((tuple, next) => {
    const auditType = degreePrograms.some(dp => isFromDegreeProgram(next, dp))
      ? 'program'
      : 'whatIf';

    if (next.darsDegreeAuditReportId === null) {
      tuple.pending[auditType]++;
      return tuple;
    }

    if (auditType === 'program') {
      tuple.programMetadata[
        next.darsDegreeAuditReportId
      ] = next as AuditMetadataWithReportId;
    } else {
      tuple.whatIfMetadata[
        next.darsDegreeAuditReportId
      ] = next as AuditMetadataWithReportId;
    }

    return tuple;
  }, empty);
};
