import { PreferencesResolver } from './../core/preferences.resolver';
import { ConstantsService } from '@app/degree-planner/services/constants.service';
import { AuditViewComponent } from './dars-audit-view/dars-audit-view.component';
import { DARSViewComponent } from './dars-view/dars-view.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: DARSViewComponent,
    resolve: { constants: ConstantsService, preferences: PreferencesResolver },
  },
  {
    path: ':darsDegreeAuditReportId',
    component: AuditViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ConstantsService, PreferencesResolver],
})
export class DarsRoutingModule {}
