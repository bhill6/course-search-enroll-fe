import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MediaMatcher } from '@angular/cdk/layout';
import { environment } from './../../../environments/environment';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import { SiteHelpDialogComponent } from '@app/shared/dialogs/site-help-dialog/site-help-dialog.component';

@Component({
  selector: 'cse-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public activeRoadmapId$: Observable<number>;
  public useNewDARSView?: boolean;
  public profileFirstname = '';

  constructor(
    private api: DegreePlannerApiService,
    private dialog: MatDialog,
    public mediaMatcher: MediaMatcher,
  ) {
    this.useNewDARSView = environment['useNewDARSView'] === true;
  }

  ngOnInit() {
    this.api.getUserProfile().subscribe(profile => {
      this.profileFirstname = profile.firstName;
    });
  }

  public openHelpDialog(): void {
    this.dialog.open(SiteHelpDialogComponent, {
      width: '500px',
      height: '400px',
      panelClass: 'site-help-dialog',
      data: {},
    });
  }
}
