export interface Profile {
  pvi: string;
  username: string;
  fullName: string;
  emailAddress: string;
  firstName: string;
  lastName: string;
  displayName: string;
}
