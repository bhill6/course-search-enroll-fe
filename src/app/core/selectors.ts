import { createSelector } from '@ngrx/store';
import { GlobalState } from './state';
import { UserPreferences } from './models/user-preferences';

export const getAllUserPreferences = createSelector(
  (state: GlobalState) => state.preferences,
  preferences => preferences,
);

export const getUserPreference = createSelector(
  (state: GlobalState) => state.preferences,
  (preferences: UserPreferences, pref: keyof UserPreferences): unknown => {
    return preferences[pref];
  },
);
