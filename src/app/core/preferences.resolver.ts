import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserPreferences } from './models/user-preferences';
import { Observable, of } from 'rxjs';
import { DegreePlannerApiService } from '../degree-planner/services/api.service';
import { Store } from '@ngrx/store';
import { GlobalState } from './state';
import { tap } from 'rxjs/operators';
import { PopulateUserPreferences } from './actions';

@Injectable({ providedIn: 'root' })
export class PreferencesResolver implements Resolve<UserPreferences> {
  private cache: UserPreferences | undefined = undefined;

  constructor(
    private api: DegreePlannerApiService,
    private store: Store<GlobalState>,
  ) {}

  public resolve(): Observable<UserPreferences> {
    if (this.cache !== undefined) {
      return of(this.cache);
    }

    return this.api.getUserPreferences().pipe(
      tap(preferences => {
        this.cache = preferences;
        this.store.dispatch(new PopulateUserPreferences({ preferences }));
      }),
    );
  }
}
